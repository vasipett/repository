package com.app.Receiver;

import com.app.Domain.User;
import com.app.Sender.Sender;
import com.app.Service.StudentService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Integer.parseInt;

@RabbitListener(queues = "c2r")
public class Receiver {

    //autowired the StudentService class
    @Autowired
    StudentService studentService;

    @Autowired
    User user;

    @Autowired
    Sender sender;

    @RabbitHandler
    public void receive(String message) {
        String[] buffer = message.split("-");

        user.setId(parseInt(buffer[0]));
        user.setCode(parseInt(buffer[1]));
        user.setName(buffer[2]);
        user.setEmail(buffer[3]);

        studentService.saveOrUpdate(user);
        System.out.println(" [x] Received '" +
                user.getId()+"-"+
                user.getCode()+"-"+
                user.getName()+"-"+
                user.getEmail()+"'");

        sender.send("created field with {" +
                "id: " + user.getId()+
                ", code: " + user.getCode()+
                ", name: " + user.getName()+
                ", email: " + user.getEmail());
    }
}
