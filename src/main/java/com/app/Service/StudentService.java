package com.app.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.Domain.User;
import com.app.Repository.UserRepository;
//defining the business logic
@Service
public class StudentService
{
    @Autowired
    UserRepository userRepository;
    //getting all student records
    public List<User> getAllStudent()
    {
        List<User> users = new ArrayList<User>();
        userRepository.findAll().forEach(student -> users.add(student));
        return users;
    }
    //getting a specific record
    public User getStudentById(int id)
    {
        return userRepository.findById(id).get();
    }
    public void saveOrUpdate(User user)
    {
        userRepository.save(user);
    }
    //deleting a specific record
    public void delete(int id)
    {
        userRepository.deleteById(id);
    }
}